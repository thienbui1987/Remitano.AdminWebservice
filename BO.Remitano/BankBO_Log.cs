﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BO.Remitano
{
    public class BankBO_Log
    {
        public int LogID { get; set; }

        public int BankID { get; set; }

        public string BankName { get; set; }

        public bool IsActive { get; set; }

        public bool IsDeleted { get; set; }

        public string CreatedUser { get; set; }

        public DateTime CreatedDate { get; set; }

        public string UpdatedUser { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string DeletedUser { get; set; }

        public DateTime DeletedDate { get; set; }

        public string IPAddress { get; set; }

        public string DeviceName { get; set; }
    }
}
