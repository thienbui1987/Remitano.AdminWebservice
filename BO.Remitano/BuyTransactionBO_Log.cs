﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BO.Remitano
{
    public class BuyTransactionBO_Log
    {
        public int LogID { get; set; }

        public int TransactionID { get; set; }

        public int MemberID { get; set; }

        public int CoinID { get; set; }

        public string TransactionCode { get; set; }

        public double CoinNumber { get; set; }

        public double Fee { get; set; }

        public int SalerID { get; set; }

        public int Status { get; set; }

        public string WalletAddress { get; set; }

        public string Mobile { get; set; }

        public string Note { get; set; }

        public DateTime TransactionDate { get; set; }

        public bool IsDeleted { get; set; }

        public string UpdatedUser { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string DeletedUser { get; set; }

        public DateTime DeletedDate { get; set; }

        public string DeletedNote { get; set; }

        public string IPAddress { get; set; }

        public string DeviceName { get; set; }
    }
}
