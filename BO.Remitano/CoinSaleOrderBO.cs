﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BO.Remitano
{
    public class CoinSaleOrderBO
    {
        public int OrderID { get; set; }

        public int CoinID { get; set; }

        public int MemberID { get; set; }

        public int ExchangeReferenceID { get; set; }

        public int PayMethodID { get; set; }

        public int TransactionTimeID { get; set; }

        public int BankID { get; set; }

        public double PriceCoinUSD { get; set; }

        public double PriceCoinUserPay { get; set; }

        public double PriceCoinBuỷeSee { get; set; }

        public double MaxPriceCoin { get; set; }

        public double MinCoinNumber { get; set; }

        public double MaxCoinNumber { get; set; }

        public int CountryID { get; set; }

        public string Unit { get; set; }

        public string AccountBankingNumber { get; set; }

        public string AccountBankingName { get; set; }

        public bool IsRefuseBuyerNotYetConfirm { get; set; }
        public int Status { get; set; }

        public bool IsDeleted { get; set; }

        public string CreatedUser { get; set; }

        public DateTime CreatedDate { get; set; }

        public string UpdatedUser { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string DeletedUser { get; set; }

        public DateTime DeletedDate { get; set; }

        public string TransactionCode { get; set; }
    }
}
