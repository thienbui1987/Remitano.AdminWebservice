﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BO.Remitano
{
    public class ExchangeReferenceBO
    {
        public int ExchangeReferenceID { get; set; }

        public string ExchangeReferenceName { get; set; }

        public string LinkReference { get; set; }

        public bool IsActive { get; set; }

        public bool IsDeleted { get; set; }

        public string CreatedUser { get; set; }

        public DateTime CreatedDate { get; set; }

        public string UpdatedUser { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string DeletedUser { get; set; }

        public DateTime DeletedDate { get; set; }
    }
}
