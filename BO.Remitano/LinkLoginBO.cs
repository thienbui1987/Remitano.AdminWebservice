﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BO.Remitano
{
    public class LinkLoginBO
    {
        public int LinkID { get; set; }

        public string Email { get; set; }

        public string LinkUrl { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime ExpiredDate { get; set; }

        public int Status { get; set; }
    }
}
