﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BO.Remitano
{
    public class MemberConfigBO
    {
        public int MemberID { get; set; }

        public bool IsSendMailHaveNewTransaction { get; set; }

        public bool IsSendMailChangeStatusTransaction { get; set; }

        public bool IsSendMailFeedBackTransaction { get; set; }

        public bool IsSendSMSHaveNewTransaction { get; set; }

        public bool IsSendSMSPaymentTransaction { get; set; }
    }
}
