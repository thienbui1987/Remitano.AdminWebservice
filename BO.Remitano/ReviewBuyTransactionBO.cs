﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BO.Remitano
{
    public class ReviewBuyTransactionBO
    {
        public int ReviewID { get; set; }

        public int TransactionID { get; set; }

        public int ReviewUserID { get; set; }

        public string ReviewContent { get; set; }

        public string ReviewEmotion { get; set; }

        public int ReviewStar { get; set; }

        public int Status { get; set; }

        public int ReviewTypeID { get; set; }

        public bool IsDeleted { get; set; }

        public string DeletedUser { get; set; }

        public DateTime DeletedDate { get; set; }
    }
}
