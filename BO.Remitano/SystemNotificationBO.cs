﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BO.Remitano
{
    public class SystemNotificationBO
    {
        public int SystemNotificationID { get; set; }

        public int SenderID { get; set; }

        public int RecieveID { get; set; }

        public int TypeNotificationID { get; set; }

        public string Content { get; set; }

        public int Status { get; set; }

        public bool IsRead { get; set; }

        public bool IsDeleted { get; set; }

        public string CreatedUser { get; set; }

        public DateTime CreatedDate { get; set; }

        public string UpdatedUser { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string DeletedUser { get; set; }

        public DateTime DeletedDate { get; set; }
    }
}
