﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Remitano.BO
{
    public class AccessRightBO
    {
        public int AdminID { get; set; }

        public int GroupID { get; set; }
    }
}
