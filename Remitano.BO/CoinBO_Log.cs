﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Remitano.BO
{
    public class CoinBO_Log
    {
        public int LogID { get; set; }
        public int CoinID { get; set; }

        public string CoinName { get; set; }

        public string Symbol { get; set; }

        public string SmartContract { get; set; }

        public long MaximumCoin { get; set; }

        public bool IsActive { get; set; }

        public bool IsDeleted { get; set; }

        public string CreatedUser { get; set; }

        public DateTime CreatedDate { get; set; }

        public string UpdatedUser { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string DeletedUser { get; set; }

        public DateTime DeletedDate { get; set; }

        public string IPAddress { get; set; }

        public string DeviceName { get; set; }
    }
}
