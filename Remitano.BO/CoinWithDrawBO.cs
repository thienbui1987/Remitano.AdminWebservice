﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Remitano.BO
{
    public class CoinWithDrawBO
    {
        public int TransactionID { get; set; }

        public int MemberID { get; set; }

        public string WalletAddress { get; set; }

        public string Note { get; set; }

        public double NumberCoin { get; set; }

        public double Fee { get; set; }

        public int Status { get; set; }

        public string TransactionCode { get; set; }

        public DateTime TransactionDate { get; set; }

        public bool IsDeleted { get; set; }

        public string CreatedUser { get; set; }

        public DateTime CreatedDate { get; set; }

        public string UpdatedUser { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string DeletedUser { get; set; }

        public DateTime DeletedDate { get; set; }

        public string DeletedNote { get; set; }
    }
}
