﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Remitano.BO
{
    public class ExchangeRateBO
    {
        public int ExchangeRateID { get; set; }

        public int CoinID { get; set; }

        public string Unit { get; set; }

        public double Values { get; set; }

        public string TokenCode { get; set; }

        public DateTime CreatedDate { get; set; }

        public bool IsActive { get; set; }
    }
}
