﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Remitano.BO
{
    public class HistoryLoginBO
    {
        public int HistoryLoginID { get; set; }

        public int MemberID { get; set; }

        public DateTime LoginDate { get; set; }

        public string IPAddress { get; set; }

        public string Device { get; set; }
    }
}
