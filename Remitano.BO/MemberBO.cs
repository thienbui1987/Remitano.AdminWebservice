﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Remitano.BO
{
    public class MemberBO
    {
        public int MemberID { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string Email { get; set; }

        public string Mobile { get; set; }

        public string Hashkey { get; set; }

        public string Address { get; set; }

        public string Avatar { get; set; }

        public DateTime Birthday { get; set; }

        public string IDCard { get; set; }

        public string Facebook { get; set; }

        public string GooglePlus { get; set; }

        public int Status { get; set; }

        public bool IsDeleted { get; set; }

        public string LinkRefer { get; set; }

        public DateTime CreatedDate { get; set; }

        public string UpdatedUser { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string DeletedUser { get; set; }

        public DateTime DeletedDate { get; set; }
    }
}
