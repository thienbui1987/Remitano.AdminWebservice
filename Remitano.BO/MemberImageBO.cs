﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Remitano.BO
{
    public class MemberImageBO
    {
        public int MemberImageID { get; set; }
        public int MemberID { get; set; }

        public string ImagePath { get; set; }
    }
}
