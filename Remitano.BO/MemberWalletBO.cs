﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Remitano.BO
{
    public class MemberWalletBO
    {
        public int WalletID { get; set; }

        public int MemberID { get; set; }

        public string WalletAddress { get; set; }

        public int CoinID { get; set; }

        public double NumberCoin { get; set; }

        public int IndexWallet { get; set; }

        public bool IsActived { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
