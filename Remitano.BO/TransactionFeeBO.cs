﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Remitano.BO
{
    public class TransactionFeeBO
    {
        public int TransactionFeeID { get; set; }

        public int CoinID { get; set; }

        public double Fee { get; set; }

        public bool IsActive { get; set; }

        public bool IsDeleted { get; set; }

        public string CreatedUser { get; set; }

        public DateTime CreatedDate { get; set; }

        public string UpdatedUser { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string DeletedUser { get; set; }

        public DateTime DeletedDate { get; set; }
    }
}
