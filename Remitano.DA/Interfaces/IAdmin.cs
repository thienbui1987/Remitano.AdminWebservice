﻿using Remitano.BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Remitano.DA.Interfaces
{
    public interface IAdmin
    {
        bool InsertAdmin(AdminBO admin);

        bool UpdateAdmin(AdminBO admin);

        bool LockAndUnlockAdmin(AdminBO admin);

        bool DeleteAdmin(AdminBO admin);

        AdminBO GetAdminDetail(string userName);

        AdminBO GetAdminDetailByID(int adminID);
    }
}
