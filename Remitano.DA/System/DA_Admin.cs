﻿using BBC.Core.Database;
using Remitano.BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.IO;
using Remitano.DA.Interfaces;

namespace Remitano.DA.System
{
    /// <summary>
    /// CREATED BY: BÙI PHƯỚC THIỆN
    /// CREATED DATE: 20.03.2018
    /// DESCRIPTION: LẤY DỮ LIỆU TỪ TABLE ADMIN
    /// </summary>
    public class DA_Admin : IAdmin
    {
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];

        /// <summary>
        /// CREATED BY: BÙI PHƯỚC THIỆN
        /// CREATED DATE: 20.03.2018
        /// DESCRIPTION: INSERT TABLE ADMIN
        /// </summary>
        public bool InsertAdmin(AdminBO admin)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            BBC.Core.Database.MySqlHelper helper = new BBC.Core.Database.MySqlHelper("",UtilityConnection.ConnectionString);
            try
            {
                bool rs = false;
                string sql = SP_INSERTADMIN_CMS;
                MySqlParameterObj[] pa = new MySqlParameterObj[12];
                pa[0] = new MySqlParameterObj("@UserName", admin.UserName, MySqlDbType.VarChar, false);
                pa[1] = new MySqlParameterObj("@Pass", admin.Password, MySqlDbType.VarChar, false);
                pa[2] = new MySqlParameterObj("@Email", admin.Email, MySqlDbType.VarChar, false);
                pa[3] = new MySqlParameterObj("@Mobile", admin.Mobile, MySqlDbType.VarChar, false);
                pa[4] = new MySqlParameterObj("@FullName", admin.FullName, MySqlDbType.VarChar, false);
                pa[5] = new MySqlParameterObj("@Avatar", admin.Avatar, MySqlDbType.VarChar, false);
                pa[6] = new MySqlParameterObj("@Address", admin.Address, MySqlDbType.VarChar, false);
                pa[7] = new MySqlParameterObj("@Birthday", admin.Birthday, MySqlDbType.DateTime, false);
                pa[8] = new MySqlParameterObj("@IsActive", admin.IsActive, MySqlDbType.Int32, false);
                pa[9] = new MySqlParameterObj("@IsDeleted", admin.IsDeleted, MySqlDbType.Int32, false);
                pa[10] = new MySqlParameterObj("@CreatedUser", admin.CreatedUser, MySqlDbType.VarChar, false);
                pa[11] = new MySqlParameterObj("@CreatedDate", admin.CreatedDate, MySqlDbType.DateTime, false);
                MySqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "Exception InsertAdmin : " + ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }


        /// <summary>
        /// CREATED BY: BÙI PHƯỚC THIỆN
        /// CREATED DATE: 20.03.2018
        /// DESCRIPTION: INSERT TABLE ADMIN
        /// </summary>
        public bool UpdateAdmin(AdminBO admin)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            BBC.Core.Database.MySqlHelper helper = new BBC.Core.Database.MySqlHelper("", UtilityConnection.ConnectionString);
            try
            {
                bool rs = false;
                string sql = SP_UPDATEADMIN_CMS;
                MySqlParameterObj[] pa = new MySqlParameterObj[9];
                pa[0] = new MySqlParameterObj("@Email", admin.Email, MySqlDbType.VarChar, false);
                pa[1] = new MySqlParameterObj("@Mobile", admin.Mobile, MySqlDbType.VarChar, false);
                pa[2] = new MySqlParameterObj("@FullName", admin.FullName, MySqlDbType.VarChar, false);
                pa[3] = new MySqlParameterObj("@Avatar", admin.Avatar, MySqlDbType.VarChar, false);
                pa[4] = new MySqlParameterObj("@Address", admin.Address, MySqlDbType.VarChar, false);
                pa[5] = new MySqlParameterObj("@Birthday", admin.Birthday, MySqlDbType.DateTime, false);
                pa[6] = new MySqlParameterObj("@AdminID", admin.AdminID, MySqlDbType.Int32, false);
                pa[7] = new MySqlParameterObj("@UpdatedUser", admin.UpdatedUser, MySqlDbType.VarChar, false);
                pa[8] = new MySqlParameterObj("@UpdatedDate", admin.UpdatedDate, MySqlDbType.DateTime, false);
                MySqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "Exception Update Admin : " + ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool LockAndUnlockAdmin(AdminBO admin)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            BBC.Core.Database.MySqlHelper helper = new BBC.Core.Database.MySqlHelper("", UtilityConnection.ConnectionString);
            try
            {
                bool rs = false;
                string sql = SP_UPDATEISACTIVEADMIN_CMS;
                MySqlParameterObj[] pa = new MySqlParameterObj[2];
                pa[0] = new MySqlParameterObj("@AdminID", admin.AdminID, MySqlDbType.Int32, false);
                pa[1] = new MySqlParameterObj("@IsActive", admin.IsActive, MySqlDbType.Int32, false);
                
                MySqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "Exception LockAndUnlockAdmin : " + ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool DeleteAdmin(AdminBO admin)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            BBC.Core.Database.MySqlHelper helper = new BBC.Core.Database.MySqlHelper("", UtilityConnection.ConnectionString);
            try
            {
                bool rs = false;
                string sql = SP_DELETEADMIN_CMS;
                MySqlParameterObj[] pa = new MySqlParameterObj[2];
                pa[0] = new MySqlParameterObj("@AdminID", admin.AdminID, MySqlDbType.Int32, false);
                pa[1] = new MySqlParameterObj("@DeletedUser", admin.DeletedUser, MySqlDbType.Int32, false);

                MySqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "Exception LockAndUnlockAdmin : " + ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }


        public AdminBO GetAdminDetail(string userName)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            BBC.Core.Database.MySqlHelper helper = new BBC.Core.Database.MySqlHelper("", UtilityConnection.ConnectionString);
            try
            {
                AdminBO objAdmin = null;
                string sql = SP_GETADMINDETAIL_CMS;
                MySqlParameterObj[] pa = new MySqlParameterObj[1];
                pa[0] = new MySqlParameterObj("@UserName", userName, MySqlDbType.VarChar, false);
               
                MySqlCommand command = helper.GetCommand(sql, pa, true);
                MySqlDataReader reader = command.ExecuteReader();
                if(reader.Read())
                {
                    objAdmin = new AdminBO();
                    objAdmin.AdminID = Int32.Parse(reader["AdminID"].ToString());
                    objAdmin.UserName = reader["UserName"].ToString();
                    objAdmin.Password = reader["Password"].ToString();
                    if(!string.IsNullOrEmpty(reader["Address"].ToString()))
                    {
                        objAdmin.Address = reader["Address"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["Avatar"].ToString()))
                    {
                        objAdmin.Avatar = reader["Avatar"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["Birthday"].ToString()))
                    {
                        objAdmin.Birthday = Convert.ToDateTime(reader["Birthday"].ToString());
                    }
                    if (!string.IsNullOrEmpty(reader["CreatedDate"].ToString()))
                    {
                        objAdmin.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    }
                    if (!string.IsNullOrEmpty(reader["CreatedUser"].ToString()))
                    {
                        objAdmin.CreatedUser = reader["CreatedUser"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["Email"].ToString()))
                    {
                        objAdmin.Email = reader["Email"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["FullName"].ToString()))
                    {
                        objAdmin.FullName = reader["FullName"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["IsActive"].ToString()))
                    {
                        objAdmin.IsActive = Convert.ToBoolean(reader["IsActive"].ToString());
                    }
                    if (!string.IsNullOrEmpty(reader["IsDeleted"].ToString()))
                    {
                        objAdmin.IsDeleted = Convert.ToBoolean(reader["IsDeleted"].ToString());
                    }
                    if (!string.IsNullOrEmpty(reader["Mobile"].ToString()))
                    {
                        objAdmin.Mobile = reader["Mobile"].ToString();
                    }
                    
                }
                return objAdmin;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "Exception GetAdminDetail : " + ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public AdminBO GetAdminDetailByID(int adminID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            BBC.Core.Database.MySqlHelper helper = new BBC.Core.Database.MySqlHelper("", UtilityConnection.ConnectionString);
            try
            {
                AdminBO objAdmin = null;
                string sql = SP_GETADMINDETAILBYID_CMS;
                MySqlParameterObj[] pa = new MySqlParameterObj[1];
                pa[0] = new MySqlParameterObj("@AdminID", adminID, MySqlDbType.Int32, false);

                MySqlCommand command = helper.GetCommand(sql, pa, true);
                MySqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    objAdmin = new AdminBO();
                    objAdmin.AdminID = Int32.Parse(reader["AdminID"].ToString());
                    objAdmin.UserName = reader["UserName"].ToString();
                    objAdmin.Password = reader["Password"].ToString();
                    if (!string.IsNullOrEmpty(reader["Address"].ToString()))
                    {
                        objAdmin.Address = reader["Address"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["Avatar"].ToString()))
                    {
                        objAdmin.Avatar = reader["Avatar"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["Birthday"].ToString()))
                    {
                        objAdmin.Birthday = Convert.ToDateTime(reader["Birthday"].ToString());
                    }
                    if (!string.IsNullOrEmpty(reader["CreatedDate"].ToString()))
                    {
                        objAdmin.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    }
                    if (!string.IsNullOrEmpty(reader["CreatedUser"].ToString()))
                    {
                        objAdmin.CreatedUser = reader["CreatedUser"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["Email"].ToString()))
                    {
                        objAdmin.Email = reader["Email"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["FullName"].ToString()))
                    {
                        objAdmin.FullName = reader["FullName"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["IsActive"].ToString()))
                    {
                        objAdmin.IsActive = Convert.ToBoolean(reader["IsActive"].ToString());
                    }
                    if (!string.IsNullOrEmpty(reader["IsDeleted"].ToString()))
                    {
                        objAdmin.IsDeleted = Convert.ToBoolean(reader["IsDeleted"].ToString());
                    }
                    if (!string.IsNullOrEmpty(reader["Mobile"].ToString()))
                    {
                        objAdmin.Mobile = reader["Mobile"].ToString();
                    }

                }
                return objAdmin;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "Exception GetAdminDetailByID : " + ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }


        public List<AdminBO> ListAdmin(string keyword, int groupID, int isActive, int isDeleted, int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            BBC.Core.Database.MySqlHelper helper = new BBC.Core.Database.MySqlHelper("", UtilityConnection.ConnectionString);
            try
            {
                List<AdminBO> lstAdmin = new List<AdminBO>();
                string sql = SP_LISTADMIN_CMS;
                MySqlParameterObj[] pa = new MySqlParameterObj[6];
                pa[0] = new MySqlParameterObj("@Keyword", keyword, MySqlDbType.VarChar, false);
                pa[1] = new MySqlParameterObj("@GroupID", groupID, MySqlDbType.Int32, false);
                pa[2] = new MySqlParameterObj("@IsActive", isActive, MySqlDbType.Int32, false);
                pa[3] = new MySqlParameterObj("@IsDeleted", isDeleted, MySqlDbType.Int32, false);
                pa[4] = new MySqlParameterObj("@StartIndex", start, MySqlDbType.Int32, false);
                pa[5] = new MySqlParameterObj("@EndIndex", end, MySqlDbType.Int32, false);
                MySqlCommand command = helper.GetCommand(sql, pa, true);
                MySqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    AdminBO objAdmin = new AdminBO();
                    objAdmin.AdminID = Int32.Parse(reader["AdminID"].ToString());
                    objAdmin.UserName = reader["UserName"].ToString();
                    objAdmin.Password = reader["Password"].ToString();
                    if (!string.IsNullOrEmpty(reader["Address"].ToString()))
                    {
                        objAdmin.Address = reader["Address"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["Avatar"].ToString()))
                    {
                        objAdmin.Avatar = reader["Avatar"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["Birthday"].ToString()))
                    {
                        objAdmin.Birthday = Convert.ToDateTime(reader["Birthday"].ToString());
                    }
                    if (!string.IsNullOrEmpty(reader["CreatedDate"].ToString()))
                    {
                        objAdmin.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    }
                    if (!string.IsNullOrEmpty(reader["CreatedUser"].ToString()))
                    {
                        objAdmin.CreatedUser = reader["CreatedUser"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["Email"].ToString()))
                    {
                        objAdmin.Email = reader["Email"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["FullName"].ToString()))
                    {
                        objAdmin.FullName = reader["FullName"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["IsActive"].ToString()))
                    {
                        objAdmin.IsActive = Convert.ToBoolean(reader["IsActive"].ToString());
                    }
                    if (!string.IsNullOrEmpty(reader["IsDeleted"].ToString()))
                    {
                        objAdmin.IsDeleted = Convert.ToBoolean(reader["IsDeleted"].ToString());
                    }
                    if (!string.IsNullOrEmpty(reader["Mobile"].ToString()))
                    {
                        objAdmin.Mobile = reader["Mobile"].ToString();
                    }

                    lstAdmin.Add(objAdmin);
                }
                return lstAdmin;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "Exception ListAdmin : " + ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        private const string SP_INSERTADMIN_CMS = "SP_INSERTADMIN_CMS";
        private const string SP_UPDATEADMIN_CMS = "SP_UPDATEADMIN_CMS";
        private const string SP_UPDATEISACTIVEADMIN_CMS = "SP_UPDATEISACTIVEADMIN_CMS";
        private const string SP_DELETEADMIN_CMS = "SP_DELETEADMIN_CMS";
        private const string SP_GETADMINDETAIL_CMS = "SP_GETADMINDETAIL_CMS";
        private const string SP_GETADMINDETAILBYID_CMS = "SP_GETADMINDETAILBYID_CMS";
        private const string SP_LISTADMIN_CMS = "SP_LISTADMIN_CMS";
    }
}
