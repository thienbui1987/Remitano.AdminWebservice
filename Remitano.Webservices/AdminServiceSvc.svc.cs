﻿using Remitano.BO;
using Remitano.DA.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Remitano.Webservices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AdminServiceSvc" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select AdminServiceSvc.svc or AdminServiceSvc.svc.cs at the Solution Explorer and start debugging.
    public class AdminServiceSvc : IAdminServiceSvc
    {
       public bool InsertAdmin(AdminBO admin)
        {
            try
            {
                DA_Admin service = new DA_Admin();
                return service.InsertAdmin(admin);
            }
            catch(Exception ex)
            {
                return false;
            }
        }
    }
}
