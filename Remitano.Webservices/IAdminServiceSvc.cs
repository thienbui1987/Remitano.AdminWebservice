﻿using Remitano.BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Remitano.Webservices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IAdminServiceSvc" in both code and config file together.
    [ServiceContract]
    public interface IAdminServiceSvc
    {
        [OperationContract]
        bool InsertAdmin(AdminBO admin);
    }
}
